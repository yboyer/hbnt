const express = require('express');
const router = express.Router();

const statusCodes = {
    _BAD_REQUEST: 400,
    _NOT_AUTHORISED: 401,
    _NOT_FOUND: 404,
    _INTERNAL_ERROR: 500
};

/**
 * Usage:
 * res.reject.[ERROR_CODE](error);
 *
 * Sends:
 * {"code": "[ERROR_CODE]"} with the associated status codes or 400
 */
router.use((req, res, next) => {
    const proxy = new Proxy({}, {
        get: (obj, code) => {
            return err => {
                console.error(`(${code}) [${res.req.method}] ${res.req.url}:`, err || new Error('Not defined'));

                /* istanbul ignore if: not supposed to happend  */
                if (code.charAt(0) !== '_') {
                    res.status(500).json({
                        message: 'Unspecified internal error'
                    });
                    throw new Error('Excepted the rejection code to start with an underscore.');
                }

                res.status(statusCodes[code] || 400);
                res.json({
                    code
                });

                /* istanbul ignore if: not supposed to happend  */
                if (!(err instanceof Error) && err !== false) {
                    throw new Error('Excepted the rejection reason to be an Error.');
                }
            };
        }
    });

    Object.freeze(proxy);

    res.reject = proxy;
    next();
});

module.exports = router;
