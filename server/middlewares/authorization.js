const passport = require('passport');

exports.requiresLogin = function(req, res, next) {
    passport.authenticate('bearer', {session: false}, (err, user) => {
        if (err || !user) {
            return res.reject._NOT_AUTHORISED(err || false);
        }

        req.user = user;
        next();
    })(req, res, next);
};
