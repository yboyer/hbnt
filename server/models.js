const MongoClient = require('mongodb').MongoClient;
const env = process.env.NODE_ENV || 'development';
const dbConfig = require('./config/database.json')[env];
if (!dbConfig) {
    throw new Error(`${env} environment is not allowed for databases.`);
}

module.exports = {
    collections: ['Tweet'],
    async init() {
        try {
            const client = await MongoClient.connect(`mongodb://${dbConfig.host}:${dbConfig.port}/${dbConfig.database}`);
            const db = client.db(dbConfig.database);
            this.collections.forEach(c => {
                this[c] = db.collection(c);
            });
        } catch (e) {
            throw e;
        }
    }
};

module.exports.collections.forEach(k => {
    module.exports[k] = {};
});
