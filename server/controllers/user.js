const UserManager = require('../libs/UserManager');
const {users} = require('../config/users.json');

module.exports = {
    signin(req, res) {
        const {login, password} = req.body;

        const user = users.find(u => u.login === login);
        if (!user) {
            return res.reject._ERROR_INVALID_IDS(new Error(`No user found for ${login}`));
        }
        if (!UserManager.authenticate(user, password)) {
            return res.reject._ERROR_INVALID_IDS(new Error(`Invalid password for ${login}`));
        }

        res.json({
            access_token: UserManager.getToken(user),
            refresh_token: UserManager.getRefreshToken(user),
            login: user.login
        });
    },

    refreshToken(req, res) {
        const user = req.user;
        if (!user) {
            return res.reject._ERROR_INVALID_PARAMS(false);
        }

        res.json({
            access_token: user.getToken(),
            refresh_token: user.getRefreshToken(),
            user: {
                id: user.id,
                login: user.login,
                phone: user.phone,
                last_name: user.last_name,
                first_name: user.first_name

            }
        });
    }
};
