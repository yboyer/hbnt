const Twit = require('twit');
const models = require('../models');
const config = require('../config');

const TwitterAPI = new Twit({
    consumer_key: config.twitter.consumer_key,
    consumer_secret: config.twitter.consumer_secret,
    access_token: config.twitter.access_token,
    access_token_secret: config.twitter.access_token_secret
});

module.exports = {
    add(req, res) {
        const id = `${req.body.id}`.trim();

        if (!id) {
            return res.reject._NOT_FOUND();
        }

        TwitterAPI.get(`/statuses/show/${id}`, (err, d) => {
            if (err) {
                return res.reject._NOT_FOUND(err);
            }

            let medias = null;
            if (d.extended_entities && d.extended_entities.media) {
                medias = d.extended_entities.media.map(m => m.media_url_https);
            }

            const data = {
                username: d.user.name,
                handle: d.user.screen_name,
                avatar: d.user.profile_image_url_https,
                date: new Date(d.created_at).getTime(),
                message: d.text,
                medias
            };

            return models.Tweet.update({_id: id}, data, {upsert: true})
                .then(() => res.status(201).end())
                .catch(res.reject._INTERNAL_ERROR);
        });
    },

    getAll(req, res) {
        return models.Tweet.find().toArray()
            .then(docs => docs.map(d => ({
                id: d._id,
                username: d.username,
                handle: d.handle,
                avatar: d.avatar,
                date: d.date,
                message: d.message,
                medias: d.medias
            })))
            .then(docs => res.json(docs))
            .catch(res.reject._INTERNAL_ERROR);
    }
};
