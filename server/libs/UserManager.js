const crypto = require('crypto');
const jwt = require('jwt-simple');
const config = require('../config');

module.exports = {
    makeSalt() {
        return `${process.env.HOSTNAME || 'HN'}${Math.round(Date.now() * Math.random())}`;
    },

    getToken(user) {
        let expires = new Date();
        expires = expires.setDate(expires.getDate() + 7);
        return jwt.encode({
            iss: user.login,
            exp: expires
        }, config.secret_key + process.env.HOSTNAME);
    },

    getRefreshToken(user) {
        let expires = new Date();
        expires = expires.setDate(expires.getDate() + 720);
        return jwt.encode({
            iss: user.login,
            exp: expires
        }, config.secret_key + process.env.HOSTNAME);
    },

    authenticate(user, text) {
        return this.encryptPassword(user.salt, text) === user.password_hash;
    },

    encryptPassword(salt, password) {
        if (!password) {
            return '';
        }

        return crypto.createHmac('sha512', salt).update(password).digest('hex');
    }
};
