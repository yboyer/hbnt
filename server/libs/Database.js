const models = require('../models');

exports.connect = () => {
    console.debug('Syncing database...');

    return models.init()
        .then(() => console.debug(`Database connected`));
};
