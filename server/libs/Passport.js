const BearerStrategy = require('passport-http-bearer').Strategy;
const {decodeToken} = require('./Bearer');
const passport = require('passport');
passport.use(new BearerStrategy((token, done) => {
    decodeToken(token)
        .then(user => done(null, user))
        .catch(err => done(err));
}));

module.exports = passport.initialize();
