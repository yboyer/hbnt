const jwt = require('jwt-simple');
const {User, Hero} = require('../models');
const config = require('../config');
const {users} = require('../config/users.json');

module.exports = {
    /**
     * Decode the bearer token
     * @param {String} token The token
     * @return {Promise} Resolve the user
     */
    async decodeToken(token) {
        const decoded = jwt.decode(token, config.secret_key + process.env.HOSTNAME);

        if (decoded.exp < Date.now()) {
            throw new Error('Token has expired');
        }

        const user = users.find(u => u.login === decoded.iss);
        if (!user) {
            throw new Error('User not found');
        }

        return user;
    }
};
