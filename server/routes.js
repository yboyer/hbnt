const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const router = express.Router();
const api = express.Router();
const {requiresLogin} = require('./middlewares/authorization');

const middlewares = {
    logger: require('./config/logger'),
    reject: require('./middlewares/reject'),
    passport: require('./libs/Passport')
};

// Controllers :
const tweet = require('./controllers/tweet');
const user = require('./controllers/user');

// Set static files routes
router.use('/', express.static(path.join(__dirname, '..', 'client')));

// Set middlewares
router.use(middlewares.logger);
router.use(middlewares.reject);
router.use(bodyParser.json());
router.use(middlewares.passport);

// API Routes
api.post('/tweets', requiresLogin, tweet.add);
api.get('/tweets', requiresLogin, tweet.getAll);
api.post('/signin', user.signin);
api.get('/refresh_token', requiresLogin, user.refreshToken);

router.use('/api', api);
router.options('*', (req, res) => res.end());
router.use((req, res) => res.status(404).end());

module.exports = router;
