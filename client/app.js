/* global Vue localStorage */

const data = {
    user: JSON.parse(localStorage.user || '{}')
};

Vue.component('signinForm', {
    template: `
<form class="form-signin" @submit.prevent="send">
    <div>
        <img class="mb-4" src="https://pbs.twimg.com/profile_images/887302702509772800/Ajj0-McE_400x400.jpg" alt="" width="72" height="72">
        <label for="inputEmail" class="sr-only">Login</label>
        <input type="text" id="inputEmail" class="form-control" v-model="login" placeholder="Login" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input type="password" id="inputPassword" class="form-control" v-model="password" placeholder="Password" required>
        <div v:if="err" class="text-danger">{{err}}</div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    </div>
</form>`,

    data: () => ({
        login: '',
        password: '',
        err: ''
    }),

    methods: {
        send() {
            this.err = '';

            this.$http.post('/api/signin', {login: this.login, password: this.password})
                .then(({body}) => {
                    localStorage.user = JSON.stringify(body);
                    data.user = body;
                })
                .catch(err => {
                    if (err.body.code === '_BAD_REQUEST') {
                        this.err = 'Invalid ids...';
                    }
                })
                .then(() => {
                    this.login = '';
                    this.password = '';
                });
        }
    }
});

Vue.component('tweets', {
    template: `
    <div class="container">
        <form class="form-inline" @submit.prevent="search">
          <div class="form-group mx-sm-3 mb-2">
            <input type="text" class="form-control" v-model="id" placeholder="Tweet ID to retrieve...">
          </div>
          <button type="submit" class="btn btn-primary mb-2" :disabled="searching">{{searching ? 'Searching...' : 'Search'}}</button>
        </form>
        <div v:if="err" class="text-danger">{{err}}</div>
        <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th scope="col">ID</th>
                  <th scope="col">Date</th>
                  <th scope="col">Username</th>
                  <th scope="col">Message</th>
                  <th scope="col">Medias</th>
                </tr>
              </thead>
              <tbody>
                  <tr v-for="tweet in tweets">
                    <td><span>{{tweet.id}}</span></td>
                    <td>{{tweet.date}}</td>
                    <td><div><img :src="tweet.avatar"></div>{{tweet.username}}</td>
                    <td>{{tweet.message}}</td>
                    <td><a v-for="media in tweet.medias" :href="media" target="_blank"><img :src="media"></a></td>
                  </tr>
              </tbody>
            </table>
        </div>
    </div>
`,

    data: () => ({
        tweets: null,
        id: '',
        err: '',
        searching: false
    }),

    created() {
        this.refresh();
    },

    methods: {
        refresh() {
            const access_token = JSON.parse(localStorage.user).access_token;
            this.$http.get('/api/tweets', {headers: {Authorization: `Bearer ${access_token}`}})
                .then(({body}) => {
                    body.forEach(d => {
                        d.date = new Date(d.date).toLocaleString();
                    });
                    this.tweets = body;
                });
        },

        search() {
            this.err = '';
            this.searching = true;

            const access_token = JSON.parse(localStorage.user).access_token;
            this.$http.post('/api/tweets', {id: this.id}, {headers: {Authorization: `Bearer ${access_token}`}})
                .then(() => {
                    this.refresh();
                })
                .catch(err => {
                    if (err.body.code === '_NOT_FOUND') {
                        this.err = `This tweet does not exist...`;
                    } else {
                        this.err = err.body.code;
                    }
                })
                .then(() => {
                    this.id = '';
                    this.searching = false;
                });
        }
    }
});

const app = new Vue({
    el: '#app',
    data: () => data,
    computed: {
        logged() {
            return Boolean(this.user.login);
        }
    },
    methods: {
        logout() {
            this.user = {};
            localStorage.user = '{}';
        }
    }
});
