# Projet Twitter
> Interface de récupération de tweets

## Usage
```sh
npm start
```


## Axes d'amélioration
- Ajouter la gestion des utilisateurs ;
- Mise en place des tests unitaires ;
- Ajouter la connexion avec mot de passe sur Mongo ;
- Utiliser les modèles de collection Mongo ;
- Améliorer l'UI / UX ;
- Restructurer le client Vue.js (webpack, séparation des composants en fichier .vue, etc).
